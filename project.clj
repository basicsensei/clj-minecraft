(defproject clj-minecraft "1.0.1-SNAPSHOT"
  :description "Clojure for Bukkit Minecraft"
  :dependencies [[org.clojure/clojure "1.4.0"]
                 [org.clojure/tools.logging "0.2.3"]
                 [swank-clojure/swank-clojure "1.3.3"]
                 [org.clojure/tools.nrepl "0.2.0-beta9"]
                 ;[org.bukkit/bukkit "1.3.2-R0.1"]
                 [org.bukkit/bukkit "1.1-R5-SNAPSHOT"]
                 ]
  ;this is ignored then?in lein 2 anyway ; :dev-dependencies [[org.bukkit/bukkit "1.1-R5-SNAPSHOT"]]

  :warn-on-reflection true
  ;:aot [cljminecraft.ClojurePlugin]
  :repl-options [:init nil :caught clj-stacktrace.repl/pst+]
  :javac-options {:destdir "classes/"}
  ;lein 2.x
  :java-source-paths ["javasrc"]
  ;lein 1.x
  ;:java-source-path "javasrc"
  
  :min-lein-version "2.0.0"
  
  :manifest {"Class-Path" "../lib/clojure.jar"}
  
  :repositories {
                 ;"spout-repo-snap" "http://repo.getspout.org/content/repositories/snapshots/"
                 ;"spout-repo-rel" "http://repo.getspout.org/content/repositories/releases/"
                 "org.bukkit" "Bukkit" "bukkit" {:url "http://repo.bukkit.org/content/groups/public/"}
                 })
